require("dotenv").config();
const mongoose = require("mongoose");
module.exports = () => {
    console.log(process.env.DB_URI);
    mongoose.connect(process.env.DB_URI,{
        user:process.env.DB_USERNAME,
        pass: process.env.DB_PASSWORD,
        dbName: process.env.DB_NAME
    });
    mongoose.connection.once('open' , () => {
        console.log('Connection established');
    })
}