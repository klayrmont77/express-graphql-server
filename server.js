let express = require("express");
// require("dotenv").config();
const { graphqlHTTP } = require("express-graphql");
const tasksResolver = require("./data/resolvers/tasks-resolver");
const tasksSchema = require("./data/schema/tasks-schema");
const createConnection = require("./db");
let app = express();
let port = process.env.PORT || 3000

app.use("/graphql" , graphqlHTTP({
    schema: tasksSchema,
    graphiql: true
}))

createConnection()

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
})