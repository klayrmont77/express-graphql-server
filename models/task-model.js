const { GraphQLID, GraphQLString } = require("graphql");
const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const Task = new Schema({
    title: String
})
module.exports = mongoose.model('Task', Task);