const { buildSchema, GraphQLObjectType, GraphQLID, GraphQLString, GraphQLList, GraphQLSchema, GraphQLNonNull, GraphQLInputObjectType } = require("graphql");
const taskModel = require("../../models/task-model");
const TaskType = new GraphQLObjectType({
    name: "Task",
    fields: () => ({
        id: {type:GraphQLID},
        title: {type:GraphQLString},
    })
})

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: () => ({
        tasks: {
            type: new GraphQLList(TaskType),
            resolve: (parent,args) => taskModel.find({})
        },
        task: {
            type: TaskType,
            args: {
                id: {type: new GraphQLNonNull(GraphQLID)}
            },
            resolve: (parent, args) => taskModel.findById(args.id)
        }
    })
})

const Mutation = new GraphQLObjectType({
    name: "MutationType",
    fields: () => ({
        deleteTask: {
            type: TaskType,
            args: {
                id: {type: new GraphQLNonNull(GraphQLID)}
            },
            resolve :(parent, args) => {
                return taskModel.findByIdAndDelete(args.id)
            }
        },
        createTask: {
            type: TaskType,
            args: {
                title: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve: (parent, args) => {
                let task = new taskModel({
                    title: args.title
                })
                return task.save();
            }
        }
    })
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})